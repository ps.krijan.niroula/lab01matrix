package com.clusus;

import com.clusus.exception.EmptyArrayMatrixException;

import java.util.Arrays;
import java.util.Objects;

public class Matrix {

    private final int[][] data;
    private final MatrixOperation matrixOperation;

    public Matrix(int[][] data) {
        Objects.requireNonNull(data);
        if (data.length == 0)
            throw new EmptyArrayMatrixException();
        this.data = data;
        matrixOperation = new MatrixOperationImpl();
    }

    public Matrix(int rowLength, int columnLength) {
        this(new int[rowLength][columnLength]);
    }

    public void setDataValueAt(int i, int j, int value) {
        this.data[i][j] = value;
    }

    public int getDataValueAt(int i, int j) {
        return this.data[i][j];
    }

    public int rowLength() {
        return data.length;
    }

    public int columnLength() {
        return getArrayColumnLength(this.data);
    }

    private int getArrayColumnLength(int[][] arr) {
        if (arr.length == 0)
            return 0;
        return arr[0].length;
    }

    public boolean isMatrixSquare() {
        return rowLength() == columnLength();
    }

    public Matrix copyMatrixSizes() {
        return new Matrix(this.rowLength(), this.columnLength());
    }

    public Matrix add(Matrix matrix) {
        Objects.requireNonNull(matrix);
        return matrixOperation.add(this, matrix);
    }

    public Matrix multiply(Matrix matrix) {
        Objects.requireNonNull(matrix);
        return matrixOperation.multiply(this, matrix);
    }

    public Matrix scalarMultiplication(int multiplier) {
        return matrixOperation.scalarMultiplication(this, multiplier);
    }

    public Matrix transpose() {
        return matrixOperation.transpose(this);
    }

    public Matrix subMatrix(int excludingRow, int excludingColumn) {
        return matrixOperation.subMatrix(this, excludingRow, excludingColumn);
    }

    public int determinant() {
        return matrixOperation.determinant(this);
    }

    public Matrix diagonalMatrix() {
        return matrixOperation.diagonalMatrix(this);
    }

    public Matrix lowerTriangularMatrix() {
        return matrixOperation.lowerTriangularMatrix(this);
    }

    public Matrix upperTriangularMatrix() {
        return matrixOperation.upperTriangularMatrix(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return Arrays.deepEquals(data, matrix.data);
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(data);
    }

}
