package com.clusus;

public interface MatrixOperation {

    Matrix add(Matrix firstMatrix, Matrix secondMatrix);

    Matrix multiply(Matrix firstMatrix, Matrix secondMatrix);

    Matrix transpose(Matrix matrix);

    Matrix scalarMultiplication(Matrix matrix, int scalarMultiplier);

    Matrix subMatrix(Matrix matrix, int excludingRow, int excludingColumn);

    int determinant(Matrix matrix);

    Matrix diagonalMatrix(Matrix matrix);

    Matrix lowerTriangularMatrix(Matrix matrix);

    Matrix upperTriangularMatrix(Matrix matrix);
}
