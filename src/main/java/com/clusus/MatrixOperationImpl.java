package com.clusus;

import com.clusus.exception.InvalidRowColumnException;
import com.clusus.exception.MatrixDimensionNotMatchedException;

public class MatrixOperationImpl implements MatrixOperation {

    private Matrix resultMatrix;

    @Override
    public Matrix add(Matrix firstMatrix, Matrix secondMatrix) {
        if (MatrixUtils.isTwoMatrixOfEqualRowsAndColumns(firstMatrix, secondMatrix)) {

            resultMatrix = firstMatrix.copyMatrixSizes();
            performMatrixAddition(firstMatrix, secondMatrix);
            return resultMatrix;
        }
        throw new MatrixDimensionNotMatchedException();
    }

    private void performMatrixAddition(Matrix firstMatrix, Matrix secondMatrix) {
        for (int i = 0; i < firstMatrix.rowLength(); i++) {
            for (int j = 0; j < firstMatrix.columnLength(); j++) {
                int sum = firstMatrix.getDataValueAt(i, j) + secondMatrix.getDataValueAt(i, j);
                resultMatrix.setDataValueAt(i, j, sum);
            }
        }
    }

    @Override
    public Matrix multiply(Matrix firstMatrix, Matrix secondMatrix) {
        if (MatrixUtils.isFirstMatrixColumnsEqualToSecondMatrixRows(firstMatrix, secondMatrix)) {

            resultMatrix = new Matrix(firstMatrix.rowLength(), secondMatrix.columnLength());
            performMatrixMultiplication(firstMatrix, secondMatrix);
            return resultMatrix;
        }
        throw new MatrixDimensionNotMatchedException();
    }

    private void performMatrixMultiplication(Matrix firstMatrix, Matrix secondMatrix) {
        for (int i = 0; i < firstMatrix.rowLength(); i++) {
            for (int j = 0; j < secondMatrix.columnLength(); j++) {
                for (int k = 0; k < firstMatrix.columnLength(); k++) {
                    int value = resultMatrix.getDataValueAt(i, j);
                    value += firstMatrix.getDataValueAt(i, k) * secondMatrix.getDataValueAt(k, j);
                    resultMatrix.setDataValueAt(i, j, value);
                }
            }
        }
    }

    @Override
    public Matrix transpose(Matrix matrix) {
        resultMatrix = new Matrix(matrix.columnLength(), matrix.rowLength());

        performMatrixTranspose(matrix);
        return resultMatrix;
    }

    private void performMatrixTranspose(Matrix matrix) {
        for (int i = 0; i < matrix.rowLength(); i++) {
            for (int j = 0; j < matrix.columnLength(); j++) {
                resultMatrix.setDataValueAt(j, i, matrix.getDataValueAt(i, j));
            }
        }
    }

    @Override
    public Matrix scalarMultiplication(Matrix matrix, int scalarMultiplier) {
        resultMatrix = matrix.copyMatrixSizes();

        performScalarMultiplication(matrix, scalarMultiplier);
        return resultMatrix;
    }

    private void performScalarMultiplication(Matrix matrix, int scalarMultiplier) {
        for (int i = 0; i < matrix.rowLength(); i++) {
            for (int j = 0; j < matrix.columnLength(); j++) {
                int value = scalarMultiplier * matrix.getDataValueAt(i, j);
                resultMatrix.setDataValueAt(i, j, value);
            }
        }
    }

    @Override
    public Matrix subMatrix(Matrix matrix, int excludingRow, int excludingColumn) {
        if (excludingRow <= matrix.rowLength() && excludingColumn <= matrix.columnLength()) {

            resultMatrix = new Matrix(matrix.rowLength() - 1, matrix.columnLength() - 1);
            performSubMatrixOperation(matrix, excludingRow, excludingColumn);
            return resultMatrix;
        }
        throw new InvalidRowColumnException();
    }

    private void performSubMatrixOperation(Matrix matrix, int excludingRow, int excludingColumn) {
        int rowCount = 0;
        int columnCount = 0;
        for (int i = 0; i < matrix.rowLength(); i++) {
            if (i == excludingRow - 1)
                continue;
            for (int j = 0; j < matrix.columnLength(); j++) {
                if (j == excludingColumn - 1)
                    continue;
                resultMatrix.setDataValueAt(rowCount, columnCount, matrix.getDataValueAt(i, j));
                columnCount++;
            }
            rowCount++;
            columnCount = 0;
        }
    }

    @Override
    public int determinant(Matrix matrix) {
        MatrixUtils.squareMatrixExceptionCheck(matrix);
        return calculateDeterminant(matrix);
    }

    private int calculateDeterminant(Matrix matrix) {
        if (MatrixUtils.isMatrixWithMinimumSize(matrix))
            return getDeterminantOfMatrixWithMinimumSize(matrix);

        return getDeterminantOfMatrixWithHigherSize(matrix);
    }

    private int getDeterminantOfMatrixWithMinimumSize(Matrix matrix) {
        int leftTopDiagonal = matrix.getDataValueAt(0, 0) * matrix.getDataValueAt(1, 1);
        int rightTopDiagonal = matrix.getDataValueAt(0, 1) * matrix.getDataValueAt(1, 0);
        return leftTopDiagonal - rightTopDiagonal;
    }

    private int getDeterminantOfMatrixWithHigherSize(Matrix matrix) {
        int determinant = 0;
        for (int j = 0; j < matrix.columnLength(); j++)
            determinant += getSubMatrixCalculation(matrix, j);
        return determinant;
    }

    private int getSubMatrixCalculation(Matrix matrix, int columnIndex) {
        Matrix subMatrix = matrix.subMatrix(1, columnIndex + 1);
        int multiplier = matrix.getDataValueAt(0, columnIndex);
        if (isOddNumber(columnIndex))
            multiplier = multiplier * (-1);

        return multiplier * calculateDeterminant(subMatrix);
    }

    private boolean isOddNumber(int value) {
        return value % 2 != 0;
    }

    @Override
    public Matrix diagonalMatrix(Matrix matrix) {
        MatrixUtils.squareMatrixExceptionCheck(matrix);

        resultMatrix = matrix.copyMatrixSizes();
        performDiagonalMatrixOperation(matrix);
        return resultMatrix;

    }

    private void performDiagonalMatrixOperation(Matrix matrix) {
        for (int i = 0; i < matrix.rowLength(); i++) {
            for (int j = 0; j < matrix.rowLength(); j++) {
                if (i == j)
                    resultMatrix.setDataValueAt(i, j, matrix.getDataValueAt(i, j));
            }
        }
    }

    @Override
    public Matrix lowerTriangularMatrix(Matrix matrix) {
        MatrixUtils.squareMatrixExceptionCheck(matrix);

        resultMatrix = matrix.copyMatrixSizes();
        performLowerTriangularOperation(matrix);
        return resultMatrix;

    }

    private void performLowerTriangularOperation(Matrix matrix) {
        for (int i = 0; i < matrix.rowLength(); i++) {
            for (int j = 0; j < matrix.rowLength(); j++) {
                if (i >= j)
                    resultMatrix.setDataValueAt(i, j, matrix.getDataValueAt(i, j));
            }
        }
    }

    @Override
    public Matrix upperTriangularMatrix(Matrix matrix) {
        MatrixUtils.squareMatrixExceptionCheck(matrix);

        resultMatrix = matrix.copyMatrixSizes();
        performUpperTriangularOperation(matrix);
        return resultMatrix;

    }

    private void performUpperTriangularOperation(Matrix matrix) {
        for (int i = 0; i < matrix.rowLength(); i++) {
            for (int j = 0; j < matrix.rowLength(); j++) {
                if (i <= j)
                    resultMatrix.setDataValueAt(i, j, matrix.getDataValueAt(i, j));
            }
        }
    }

}
