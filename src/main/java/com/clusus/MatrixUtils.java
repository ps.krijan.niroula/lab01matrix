package com.clusus;

import com.clusus.exception.NotSquareMatrixException;

public class MatrixUtils {

    private MatrixUtils() {
    }

    public static boolean isMatrixWithMinimumSize(Matrix matrix) {
        return matrix.rowLength() == 2 && matrix.columnLength() == 2;
    }

    public static void squareMatrixExceptionCheck(Matrix matrix) {
        if (!matrix.isMatrixSquare()) {
            throw new NotSquareMatrixException();
        }
    }

    public static boolean isTwoMatrixOfEqualRowsAndColumns(Matrix firstMatrix, Matrix secondMatrix) {
        return firstMatrix.rowLength() == secondMatrix.rowLength() && firstMatrix.columnLength() == secondMatrix.columnLength();
    }

    public static boolean isFirstMatrixColumnsEqualToSecondMatrixRows(Matrix firstMatrix, Matrix secondMatrix) {
        return firstMatrix.columnLength() == secondMatrix.rowLength();
    }

}
