package com.clusus;

import com.clusus.exception.InvalidRowColumnException;
import com.clusus.exception.MatrixDimensionNotMatchedException;
import com.clusus.exception.NotSquareMatrixException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MatrixOperationTest {

    private MatrixOperation matrixOperation;

    @BeforeAll
    void initialize() {
        matrixOperation = new MatrixOperationImpl();
    }

    @Test
    void givenTwoMatrixHasEqualRowsAndColumns_whenAdd_thenReturnNewMatrix() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{6, 0, 1}, {1, 2, 4}};
        Matrix secondMatrix = new Matrix(sData);
        int[][] sumData = {{7, 2, 4}, {3, 5, 8}};
        Matrix sumMatrix = new Matrix(sumData);
        assertEquals(sumMatrix, matrixOperation.add(firstMatrix, secondMatrix));
    }

    @Test
    void givenTwoMatrixHasUnequalRowsAndColumns_whenAdd_thenThrowException() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{6, 0, 1, 10}, {1, 2, 4, 100}};
        Matrix secondMatrix = new Matrix(sData);

        assertThrows(MatrixDimensionNotMatchedException.class, () -> matrixOperation.add(firstMatrix, secondMatrix));
    }

    @Test
    void givenTwoMatrixFirstMatrixColumnsEqualToSecondMatrixRows_whenMultiply_thenReturnNewMatrix() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{1, 2}, {2, 3}, {3, 4}};
        Matrix secondMatrix = new Matrix(sData);
        int[][] mData = {{14, 20}, {20, 29}};
        Matrix mMatrix = new Matrix(mData);
        assertEquals(mMatrix, matrixOperation.multiply(firstMatrix, secondMatrix));
    }

    @Test
    void givenTwoMatrixFirstMatrixColumnsNotEqualToSecondMatrixRows_whenMultiply_thenThrowException() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{6, 0, 1}, {1, 2, 4}};
        Matrix secondMatrix = new Matrix(sData);
        assertThrows(MatrixDimensionNotMatchedException.class, () -> matrixOperation.multiply(firstMatrix, secondMatrix));
    }

    @Test
    void givenMatrix_whenTranspose_thenReturnNewMatrix() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] tData = {{1, 2}, {2, 3}, {3, 4}};
        Matrix tMatrix = new Matrix(tData);
        assertEquals(tMatrix, matrixOperation.transpose(firstMatrix));
    }

    @Test
    void givenMatrix_whenPerformingSubMatrixExcludingValidRowAndColumn_thenReturnNewMatrix() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] subData = {{2, 3}};
        Matrix subMatrix = new Matrix(subData);
        assertEquals(subMatrix, matrixOperation.subMatrix(firstMatrix, 1, 3));
    }

    @Test
    void givenMatrix_whenPerformingSubMatrixExcludingInvalidRowAndColumn_thenThrowException() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        assertThrows(InvalidRowColumnException.class, () -> matrixOperation.subMatrix(firstMatrix, 1, 5));
    }

    @Test
    void givenMatrix_whenScalarMultiplicationWithScalarMultiplier_thenReturnNewMatrix() {
        int[][] data = {{1, 2, 3}, {2, 1, 4}, {2, 1, 3}};
        Matrix matrix = new Matrix(data);
        int[][] sData = {{3, 6, 9}, {6, 3, 12}, {6, 3, 9}};
        Matrix sMatrix = new Matrix(sData);
        assertEquals(sMatrix, matrixOperation.scalarMultiplication(matrix, 3));
    }

    @Test
    void givenMatrixWhichIsSquare_whenCalculatingDeterminant_thenReturnIntegerValue() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}, {22, 10, 13}};
        Matrix squareMatrix = new Matrix(data);
        assertEquals(50, matrixOperation.determinant(squareMatrix));
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenCalculatingDeterminant_thenThrowException() {
        int[][] data = {{100, 2, 3}, {200, 1, 4}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, () -> matrixOperation.determinant(matrix));
    }

    @Test
    void givenMatrixWhichIsSquare_whenDiagonalMatrix_thenReturnNewMatrix() {
        int[][] data = {{1, 12, 10}, {22, 1, 14}, {22, 10, 3}};
        Matrix squareMatrix = new Matrix(data);
        int[][] dData = {{1, 0, 0}, {0, 1, 0}, {0, 0, 3}};
        Matrix dMatrix = new Matrix(dData);
        assertEquals(dMatrix, matrixOperation.diagonalMatrix(squareMatrix));
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenDiagonalMatrix_thenReturnNewMatrix() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, () -> matrixOperation.diagonalMatrix(matrix));
    }

    @Test
    void givenMatrixWhichIsSquare_whenLowerTriangularMatrix_thenReturnNewMatrix() {
        int[][] data = {{1, 2, 3}, {2, 1, 4}, {2, 1, 3}};
        Matrix squareMatrix = new Matrix(data);
        int[][] lData = {{1, 0, 0}, {2, 1, 0}, {2, 1, 3}};
        Matrix lMatrix = new Matrix(lData);
        assertEquals(lMatrix, matrixOperation.lowerTriangularMatrix(squareMatrix));
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenLowerTriangularMatrix_thenThrowException() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, () -> matrixOperation.lowerTriangularMatrix(matrix));
    }

    @Test
    void givenMatrixWhichIsSquare_whenUpperTriangularMatrix_thenReturnNewMatrix() {
        int[][] data = {{1, 2, 3}, {6, 1, 4}, {2, 8, 3}};
        Matrix squareMatrix = new Matrix(data);
        int[][] uData = {{1, 2, 3}, {0, 1, 4}, {0, 0, 3}};
        Matrix uMatrix = new Matrix(uData);
        assertEquals(uMatrix, matrixOperation.upperTriangularMatrix(squareMatrix));
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenUpperTriangularMatrix_thenThrowException() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, () -> matrixOperation.upperTriangularMatrix(matrix));
    }

}