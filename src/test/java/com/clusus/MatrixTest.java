package com.clusus;

import com.clusus.exception.EmptyArrayMatrixException;
import com.clusus.exception.InvalidRowColumnException;
import com.clusus.exception.MatrixDimensionNotMatchedException;
import com.clusus.exception.NotSquareMatrixException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MatrixTest {

    private MatrixOperation matrixOperation;

    @BeforeAll
    void initialize() {
        matrixOperation = new MatrixOperationImpl();
    }

    @Test
    void whenDataIsEmpty_thenThrowException() {
        assertThrows(EmptyArrayMatrixException.class, () -> new Matrix(new int[][]{}));
    }

    @Test
    void whenDataHasValue_thenInitializeMatrix() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix matrix = new Matrix(data);
        assertAll(
                () -> assertEquals(2, matrix.rowLength()),
                () -> assertEquals(3, matrix.columnLength())
        );
    }

    @Test
    void givenTwoMatrixWithSameData_whenComparing_thenShouldBeEqual() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        int[][] sData = {{1, 2, 3}, {2, 3, 4}};
        assertEquals(new Matrix(data), new Matrix(sData));
    }

    @Test
    void givenTwoMatrixHasEqualRowsAndColumns_whenAdd_thenShouldEqualToMatrixOperationAdd() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{6, 0, 1}, {1, 2, 4}};
        Matrix secondMatrix = new Matrix(sData);
        assertEquals(matrixOperation.add(firstMatrix, secondMatrix), firstMatrix.add(secondMatrix));
    }

    @Test
    void givenTwoMatrixHasUnequalRowsAndColumns_whenAdd_thenThrowException() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{6, 0, 1, 10}, {1, 2, 4, 100}};
        Matrix secondMatrix = new Matrix(sData);

        assertThrows(MatrixDimensionNotMatchedException.class, () -> firstMatrix.add(secondMatrix));
    }

    @Test
    void givenTwoMatrixFirstMatrixColumnsEqualToSecondMatrixRows_whenMultiply_thenShouldEqualToMatrixOperationMultiply() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{1, 2}, {2, 3}, {3, 4}};
        Matrix secondMatrix = new Matrix(sData);
        assertEquals(matrixOperation.multiply(firstMatrix, secondMatrix), firstMatrix.multiply(secondMatrix));
    }

    @Test
    void givenTwoMatrixFirstMatrixColumnsNotEqualToSecondMatrixRows_whenMultiply_thenThrowException() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        int[][] sData = {{6, 0, 1}, {1, 2, 4}};
        Matrix secondMatrix = new Matrix(sData);
        assertThrows(MatrixDimensionNotMatchedException.class, () -> firstMatrix.multiply(secondMatrix));
    }

    @Test
    void givenMatrix_whenTranspose_thenShouldEqualToMatrixOperationTranspose() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        assertEquals(matrixOperation.transpose(firstMatrix), firstMatrix.transpose());
    }

    @Test
    void givenMatrix_whenPerformingSubMatrixExcludingValidRowAndColumn_thenShouldEqualToMatrixOperationSubMatrix() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        assertEquals(matrixOperation.subMatrix(firstMatrix, 1, 3), firstMatrix.subMatrix(1, 3));
    }

    @Test
    void givenMatrix_whenPerformingSubMatrixExcludingInvalidRowAndColumn_thenThrowException() {
        int[][] data = {{1, 2, 3}, {2, 3, 4}};
        Matrix firstMatrix = new Matrix(data);
        assertThrows(InvalidRowColumnException.class, () -> firstMatrix.subMatrix(1, 5));
    }

    @Test
    void givenMatrix_whenScalarMultiplicationWithScalarMultiplier_thenShouldEqualToMatrixOperationScalarMultiplication() {
        int[][] data = {{1, 2, 3}, {2, 1, 4}, {2, 1, 3}};
        Matrix matrix = new Matrix(data);
        assertEquals(matrixOperation.scalarMultiplication(matrix, 3), matrix.scalarMultiplication(3));
    }

    @Test
    void givenMatrixWhichIsSquare_whenCalculatingDeterminant_thenShouldEqualToMatrixOperationDeterminant() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}, {22, 10, 13}};
        Matrix squareMatrix = new Matrix(data);
        assertEquals(matrixOperation.determinant(squareMatrix), squareMatrix.determinant());
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenCalculatingDeterminant_thenThrowException() {
        int[][] data = {{100, 2, 3}, {200, 1, 4}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, matrix::determinant);
    }

    @Test
    void givenMatrixWhichIsSquare_whenDiagonalMatrix_thenShouldEqualToMatrixOperationDiagonal() {
        int[][] data = {{1, 12, 10}, {22, 1, 14}, {22, 10, 3}};
        Matrix squareMatrix = new Matrix(data);
        assertEquals(matrixOperation.diagonalMatrix(squareMatrix), squareMatrix.diagonalMatrix());
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenDiagonalMatrix_thenThrowException() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, matrix::diagonalMatrix);
    }

    @Test
    void givenMatrixWhichIsSquare_whenLowerTriangularMatrix_thenShouldEqualToMatrixOperationLowerTriangular() {
        int[][] data = {{1, 2, 3}, {2, 1, 4}, {2, 1, 3}};
        Matrix squareMatrix = new Matrix(data);
        assertEquals(matrixOperation.lowerTriangularMatrix(squareMatrix), squareMatrix.lowerTriangularMatrix());
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenLowerTriangularMatrix_thenThrowException() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, matrix::lowerTriangularMatrix);
    }

    @Test
    void givenMatrixWhichIsSquare_whenUpperTriangularMatrix_thenShouldEqualToMatrixOperationUpperTriangular() {
        int[][] data = {{1, 2, 3}, {6, 1, 4}, {2, 8, 3}};
        Matrix squareMatrix = new Matrix(data);
        assertEquals(matrixOperation.upperTriangularMatrix(squareMatrix), squareMatrix.upperTriangularMatrix());
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenUpperTriangularMatrix_thenThrowException() {
        int[][] data = {{2, 12, 10}, {22, 11, 14}};
        Matrix matrix = new Matrix(data);
        assertThrows(NotSquareMatrixException.class, matrix::upperTriangularMatrix);
    }
}