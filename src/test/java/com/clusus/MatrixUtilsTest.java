package com.clusus;

import com.clusus.exception.NotSquareMatrixException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixUtilsTest {

    @Test
    void givenMatrixWithRowsAndColumnsSizeEqualToTwo_whenCheckingMinimumSize_thenReturnTrue() {
        int[][] data = {{1, 2}, {4, 5}};
        Matrix matrix = new Matrix(data);
        assertTrue(MatrixUtils.isMatrixWithMinimumSize(matrix));
    }

    @Test
    void givenMatrixWithRowsAndColumnsSizeNotEqualToTwo_whenCheckingMinimumSize_thenReturnFalse() {
        int[][] data = {{1, 2, 3}, {4, 5, 6}};
        Matrix matrix = new Matrix(data);
        assertFalse(MatrixUtils.isMatrixWithMinimumSize(matrix));
    }

    @Test
    void givenMatrixWhichIsNotSquare_whenSquareExceptionCheck_thenThrowException() {
        int[][] data = {{1, 2, 3}, {4, 5, 6}};
        Matrix matrix = new Matrix(data);

        assertThrows(NotSquareMatrixException.class, () -> MatrixUtils.squareMatrixExceptionCheck(matrix));
    }

    @Test
    void givenTwoMatrixOfEqualRowsAndColumns_whenComparingRowsAndColumns_thenReturnTrue() {
        int[][] fData = {{1, 2, 3}, {4, 5, 6}};
        Matrix firstMatrix = new Matrix(fData);
        int[][] sData = {{10, 22, 32}, {44, 51, 60}};
        Matrix secondMatrix = new Matrix(sData);

        assertTrue(MatrixUtils.isTwoMatrixOfEqualRowsAndColumns(firstMatrix, secondMatrix));
    }

    @Test
    void givenTwoMatrixOfUnEqualRowsAndColumns_whenComparingRowsAndColumns_thenReturnFalse() {
        int[][] fData = {{1, 2, 3, 11}, {4, 5, 6, 9}};
        Matrix firstMatrix = new Matrix(fData);
        int[][] sData = {{10, 22, 32}, {44, 51, 60}};
        Matrix secondMatrix = new Matrix(sData);

        assertFalse(MatrixUtils.isTwoMatrixOfEqualRowsAndColumns(firstMatrix, secondMatrix));
    }

    @Test
    void givenFirstMatrixColumnsEqualToSecondMatrixRows_whenComparing_thenReturnTrue() {
        int[][] fData = {{1, 2, 3}, {4, 5, 6}};
        Matrix firstMatrix = new Matrix(fData);
        int[][] sData = {{10, 22, 32}, {44, 51, 60}, {20, 60, 70}};
        Matrix secondMatrix = new Matrix(sData);

        assertTrue(MatrixUtils.isFirstMatrixColumnsEqualToSecondMatrixRows(firstMatrix, secondMatrix));
    }

    @Test
    void givenFirstMatrixColumnsNotEqualToSecondMatrixRows_whenComparing_thenReturnFalse() {
        int[][] fData = {{1, 2, 3}, {4, 5, 6}};
        Matrix firstMatrix = new Matrix(fData);
        int[][] sData = {{10, 22, 32}, {44, 51, 60}};
        Matrix secondMatrix = new Matrix(sData);

        assertFalse(MatrixUtils.isFirstMatrixColumnsEqualToSecondMatrixRows(firstMatrix, secondMatrix));
    }
}